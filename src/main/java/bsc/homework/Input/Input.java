package bsc.homework.Input;

import bsc.homework.Data.Package;
import bsc.homework.Data.PackageBuilder;
import bsc.homework.Data.PackagesProcessed;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * Handles the input of the user and file input,
 * if there is wrong input it logs it.
 */
@Log
@Component
public class Input {
@Resource
PackageBuilder packageBuilder;
@Resource
PackagesProcessed packagesProcessed;

public void readPackageInput(InputStream inputStream) {
	Scanner scanner = new Scanner(inputStream).useLocale(Locale.US);
	String line;
	while (!(line = scanner.nextLine()).contains("quit")) {
		Scanner lineScan = new Scanner(new ByteArrayInputStream(line.getBytes())).useLocale(Locale.US);
		Package newPackage;
		try {
			newPackage = packageBuilder.createPackage(lineScan.nextDouble(), lineScan.nextInt());
		} catch (NumberFormatException | NoSuchElementException ex) {
			log.severe("Wrong input");
			continue;
		}
		packagesProcessed.addPackage(newPackage);
	}
}

	public void readFilePackageInput(InputStream fileInputStream){
		Scanner scanner = new Scanner(fileInputStream).useLocale(Locale.US);
		while (scanner.hasNextLine()) {
			Package newPackage;
			try {
				newPackage = packageBuilder.createPackage(scanner.nextDouble(), scanner.nextInt());
			} catch (NumberFormatException ex) {
				log.severe("Wrong input");
				continue;
			}
			packagesProcessed.addPackage(newPackage);
		}
	}

	public void readFileFeeInput(InputStream fileInputStream) {
		Scanner scanner = new Scanner(fileInputStream).useLocale(Locale.US);
		while (scanner.hasNextLine()) {
			try {
				packageBuilder.addFee(scanner.nextDouble(), scanner.nextDouble());
			} catch (NumberFormatException ex) {
				log.severe("Wrong input");
			}
		}
	}
}

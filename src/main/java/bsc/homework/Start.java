package bsc.homework;

import bsc.homework.Input.Input;
import lombok.extern.java.Log;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;


/**
 * Starts the application and checks if there weren't any initial data or fee file
 */
@Component
@Log
public class Start implements ApplicationRunner {
@Resource
private Input input;
@Resource
private ApplicationContext appContext;

@Override
public void run(ApplicationArguments args) throws Exception {
	if (args.containsOption("FeeFile")) {
		log.info("Adding feeFile:" + args.getOptionValues("FeeFile").get(0));
		File feeFile = new File(args.getOptionValues("FeeFile").get(0));
		input.readFileFeeInput(new FileInputStream(feeFile));

	} else {
		log.warning("No fee file added");
	}
	if (args.containsOption("InitialData")) {
		log.info("Adding initialDataFile:" + args.getOptionValues("InitialData").get(0));
		File initialDataFile = new File(args.getOptionValues("InitialData").get(0));
		input.readFilePackageInput(new FileInputStream(initialDataFile));
	}
	System.out.println("Print packages that are processed");
	input.readPackageInput(System.in);

	SpringApplication.exit(appContext, () -> 0);


}
}

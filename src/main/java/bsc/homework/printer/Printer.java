package bsc.homework.printer;

import bsc.homework.Data.PackagesProcessed;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * Generates output of packages processed every minute.
 */
@Component
public class Printer {
	@Resource
	PackagesProcessed packagesProcessed;


	@Scheduled(cron = "0 * * * * *")
	public void print(){
		System.out.println(packagesProcessed);
	}

}

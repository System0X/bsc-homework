package bsc.homework.Data;

import org.junit.jupiter.api.Test;

public class PackageProcessedTest {
    /**
     * Add Package
     */
        @Test
    public void add() {
        PackagesProcessed packagesProcessed = new PackagesProcessed();
        assert packagesProcessed.packages.isEmpty();

        Package package1 = new Package(1, 10.5, 100.1);
        packagesProcessed.addPackage(package1);

        assert !packagesProcessed.packages.isEmpty();

        for (Package package1Test : packagesProcessed.packages) {
            package1Test.equals(package1);
        }


    }

    /**
     * Package string generation and sort by weight
     */
    @Test
    public void stringGeneration() {
        PackagesProcessed packagesProcessed1 = new PackagesProcessed();

        Package package1 = new Package(1, 10.5, 100.1);
        Package package2 = new Package(2, 11.5, 100.1);

        packagesProcessed1.addPackage(package2);
        packagesProcessed1.addPackage(package1);

        assert packagesProcessed1.toString().equals(package2.toString() + System.lineSeparator() + package1.toString() + System.lineSeparator());

        PackagesProcessed packagesProcessed2 = new PackagesProcessed();

        Package package3 = new Package(3, 10.5, 100.1);
        Package package4 = new Package(4, 11.5, 100.1);

        packagesProcessed2.addPackage(package3);
        packagesProcessed2.addPackage(package4);


        assert packagesProcessed2.toString().equals(package4.toString() + System.lineSeparator() + package3.toString() + System.lineSeparator());

    }

}


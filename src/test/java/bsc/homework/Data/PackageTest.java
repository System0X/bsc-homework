package bsc.homework.Data;

import org.junit.jupiter.api.Test;

public class PackageTest {
    /**
     * Test string generation
     */
    @Test
    public void stringGeneration() {
        Package package1 = new Package(1, 10.5, 100.1);
        Package package2 = new Package(10000, 10.5, Double.NaN);

        assert package1.toString().equals("00001 10.500 100.10");
        assert package2.toString().equals("10000 10.500");


    }

}

package bsc.homework.Data;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Vector;


/**
 * This class holds data about packages
 */
@NoArgsConstructor
@Component
public class PackagesProcessed {

	Vector<Package> packages = new Vector<>();

	public void addPackage(Package newPackage){

		packages.add(newPackage);
	}

@Override
public synchronized String toString() {

		packages.sort(Comparator.comparing(Package::getWeight).reversed());
		StringBuilder text = new StringBuilder();
	for(Package onePackage : packages){
		text.append(onePackage.toString());
		text.append(System.lineSeparator());
	}

	return text.toString();
}
}

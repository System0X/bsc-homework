package bsc.homework.Data;

import org.junit.jupiter.api.Test;

import java.util.Map;

public class PackageBuilderTest {

    /**
     * Test fee finding
     */
        @Test
    public void FeeFinding() {
        PackageBuilder packageBuilder = new PackageBuilder();

        packageBuilder.addFee(10., 10.);
        packageBuilder.addFee(20., 20.);

        assert packageBuilder.findFee(5.) == 0.;
        assert packageBuilder.findFee(9.) == 0.;
        assert packageBuilder.findFee(10.) == 10.;
        assert packageBuilder.findFee(19.) == 10.;
        assert packageBuilder.findFee(20.) == 20.;
        assert packageBuilder.findFee(2000.) == 20.;

    }

    /**
     * Create package with no fee
     */
    @Test

    public void NoFee() {
        PackageBuilder packageBuilder = new PackageBuilder();
        Package newPackage = packageBuilder.createPackage(10.5, 500);
        assert newPackage.getFee().isNaN();
        assert newPackage.getPostalCode() == 500;
        assert newPackage.getWeight() == 10.5;

    }

    /**
     * Create package with fee
     */
    @Test

    public void Fee() {
        PackageBuilder packageBuilder = new PackageBuilder();
        packageBuilder.addFee(10., 10.);
        Package newPackage = packageBuilder.createPackage(10.5, 500);

        assert newPackage.getFee() == 10;
        assert newPackage.getPostalCode() == 500;
        assert newPackage.getWeight() == 10.5;

    }

    /**
     * Test fee adding
     */
    @Test
    public void Adding() {
        PackageBuilder packageBuilder = new PackageBuilder();
        assert packageBuilder.getFees().isEmpty();
        packageBuilder.addFee(10., 100.);
        assert !packageBuilder.getFees().isEmpty();

        for (Map.Entry<Double, Double> fee : packageBuilder.getFees().entrySet()) {
            assert fee.getKey() == 10;
            assert fee.getValue() == 100;
        }


    }

}
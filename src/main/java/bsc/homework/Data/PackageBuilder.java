package bsc.homework.Data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Builder for package, can add fee for packages to have fees
 * fees will not be added retrospectively.
 */
@Component
@NoArgsConstructor
public class PackageBuilder {


/**
 * fees is Map< Weight, price>
 */
@Getter
private final Map<Double, Double> fees = new HashMap<>();

/**
 * Checks if there are any fees. If not it returns NaN.
 *
 * If there are any fees it outputs the largest fee that is smaller then the inputted weight
 * or returns 0 if weight is smaller then the smallest fee
 *
 * @param weight weight of a package
 * @return Fee to be charged on that package
 */
	public Double findFee(Double weight){
		if(fees.isEmpty()){
			return Double.NaN;
		}
		Optional<Map.Entry<Double, Double>> fee = fees.entrySet()
				.stream()
				.filter((entery) -> entery.getKey() <= weight)
				.max(Map.Entry.comparingByKey());
		if(fee.isPresent()){
			return fee.get().getValue();
		}else{
			return (double) 0;
		}
	}

	public Package createPackage(Double weight, Integer postalCode){
		if(fees.isEmpty()){
			return new Package(postalCode,weight,Double.NaN);
		}
		return new Package(postalCode,weight,findFee(weight));
	}

	public void addFee(Double weight,Double price){
		if(fees.containsKey(weight)){
			fees.replace(weight, price, price);
		}else {
			fees.put(weight, price);
		}
	}


}

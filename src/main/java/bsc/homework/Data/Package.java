package bsc.homework.Data;

import lombok.*;

import java.util.Locale;


/**
 * Package is a unit of information it holds everything about a package, postal code , weight, fee(if there are any fees)
 * Package is immutable and non null.
 *
 * */
@Value
public class Package {
	@NonNull
	Integer postalCode;
	@NonNull
	Double weight;
	@NonNull
	Double fee;

	@Override
	public String toString() {
		String string=String.format("%05d",postalCode) +" "+String.format(Locale.US,"%.3f",weight);
		if(fee.isNaN()){

			return string;
		}else {
			return string+" "+String.format(Locale.US,"%.2f",fee);
		}
	}
}
